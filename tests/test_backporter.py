"""Tests for backporter."""
from subprocess import CalledProcessError
from unittest import TestCase
from unittest import mock

from webhook import common
from webhook import libbackport
from webhook.libjira import JiraField
from webhook.session import SessionRunner
from webhook.utils import backporter


def backport_session_runner(
    jira=None,
    args_str='--rhkernel-src /src/kernel-ark -j https://issues.redhat.com/browse/RHEL-101'.split()
) -> SessionRunner:
    """Return a BaseSession with some fake bits."""
    parser = common.get_arg_parser('BACKPORTER')
    parser.add_argument('-r', '--rhkernel-src',
                        help='Directory containing RH kernel projects git tree')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode, omit certain checks")
    args = parser.parse_args(args_str)
    session = SessionRunner.new('backporter', args=args)
    session.jira = jira or mock.Mock()
    session.get_gl_instance = mock.Mock()
    return session


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'})
class TestLibBackporter(TestCase):
    """Tests for libbackport library functions."""

    @mock.patch('webhook.kgit.worktree_add', mock.Mock(return_value=True))
    @mock.patch('webhook.libbackport.Repo', mock.Mock(return_value=True))
    def test_repr(self):
        mock_project = mock.Mock(spec_set=['name', 'namespace'])
        mock_project.name = 'rhel-8-sandbox'
        mock_project.namespace = 'redhat/rhel/kernel/src/rhel-8-sandbox'
        mock_branch = mock.Mock(spec_set=['name'])
        mock_branch.name = 'main'
        mock_issue = mock.Mock(spec_set=['key'])
        mock_issue.key = 'RHEL-101'
        mock_rhissue = mock.Mock(spec_set=['id', 'issue', 'ji_project', 'ji_branch', 'jira_link',
                                           'ji_fix_version', 'ji_commit_hashes'])
        mock_rhissue.id = mock_issue.key
        mock_rhissue.issue = mock_issue
        mock_rhissue.ji_project = mock_project
        mock_rhissue.ji_branch = mock_branch
        mock_rhissue.ji_fix_version = 'rhel-8.10'
        mock_rhissue.ji_commit_hashes = ['abcdef0123456789']
        mock_rhissue.jira_link = 'https://issues.redhat.com/browse/RHEL-101'
        mock_bp = libbackport.Backport(mock_rhissue, '/src/kernel-ark')

        mock_repo = mock.Mock(spec_set=['active_branch', 'working_tree_dir'])
        mock_repo.active_branch = mock.Mock(spec_set=['name'])
        mock_repo.active_branch.name = 'backport-RHEL-101-rhel-8-sandbox-main'
        mock_repo.working_tree_dir = '/src/backport-RHEL-101-rhel-8-sandbox-main'
        mock_bp.repo = mock_repo

        self.assertIn(('Backport attempt:\n'
                       ' * Issue: https://issues.redhat.com/browse/RHEL-101 '
                       '(Fix Version: rhel-8.10)\n'
                       ' * Target branch: rhel-8-sandbox/main\n'
                       ' * Project namespace: redhat/rhel/kernel/src/rhel-8-sandbox\n'
                       ' * Backport directory: /src/backport-RHEL-101-rhel-8-sandbox-main\n'
                       ' * Source branch: backport-RHEL-101-rhel-8-sandbox-main\n'
                       ' * Commit(s): [\'abcdef0123456789\']'),
                      f'{mock_bp}')

    @mock.patch('webhook.kgit.branch_delete')
    @mock.patch('webhook.kgit.worktree_add')
    @mock.patch('webhook.libbackport.Repo')
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_prep_branch_for_backporting(self, mock_gitrepo, mock_gwta, mock_gbd):
        mock_project = mock.Mock(spec_set=['name'])
        mock_project.name = 'rhel-8-sandbox'
        mock_branch = mock.Mock(spec_set=['name'])
        mock_branch.name = 'main'
        mock_issue = mock.Mock(spec_set=['key'])
        mock_issue.key = 'RHEL-101'
        mock_rhissue = mock.Mock(spec_set=['id', 'issue', 'ji_project', 'ji_branch',
                                           'ji_commit_hashes', 'jira_link'])
        mock_rhissue.id = mock_issue.key
        mock_rhissue.issue = mock_issue
        mock_rhissue.ji_project = mock_project
        mock_rhissue.ji_branch = mock_branch
        mock_rhissue.ji_commit_hashes = []
        mock_rhissue.jira_link = 'https://issues.redhat.com/browse/RHEL-101'
        mock_repo = mock.Mock(spec_set=['active_branch', 'working_tree_dir'])
        mock_repo.active_branch = mock.Mock(spec_set=['name'])
        mock_repo.active_branch.name = 'backport-RHEL-101-rhel-8-sandbox-main'
        mock_repo.working_tree_dir = '/src/backport-RHEL-101-rhel-8-sandbox-main'
        mock_gitrepo.return_value = mock_repo
        mock_gwta.side_effect = CalledProcessError(2, ["bad"])
        mock_gbd.side_effect = CalledProcessError(2, ["bad"])
        with self.assertLogs('cki.webhook.libbackport', level='INFO') as logs:
            mock_bp = libbackport.Backport(mock_rhissue, '/src/kernel-ark')
            self.assertEqual(mock_bp.repo.active_branch.name,
                             'backport-RHEL-101-rhel-8-sandbox-main')
            self.assertEqual(mock_bp.repo.working_tree_dir,
                             '/src/backport-RHEL-101-rhel-8-sandbox-main')
            self.assertIn("Worktree for RHEL-101 backport already exists", logs.output[-1])


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'})
class TestBackporter(TestCase):
    """Tests for the various backporter functions."""

    @mock.patch('webhook.kgit.fetch_all', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.worktree_remove', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.branch_delete', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.cherry_pick')
    @mock.patch('webhook.libbackport.Repo')
    @mock.patch('webhook.libbackport.Repo', mock.Mock(return_value=True))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_try_to_backport(self, mock_repo, mock_gcp):
        mock_session = backport_session_runner()
        mock_project = mock.Mock(spec_set=['name', 'namespace'])
        mock_project.name = 'rhel-8-sandbox'
        mock_project.namespace = 'redhat/rhel/kernel/src/rhel-8-sandbox'
        mock_branch = mock.Mock(spec_set=['name'])
        mock_branch.name = 'main'
        mock_issue = mock.Mock(spec_set=['key', 'fields'])
        mock_issue.key = 'RHEL-101'
        mock_fields = mock.Mock(spec_set=['summary', 'labels', JiraField.Commit_Hashes])
        mock_fields.summary = "jira bug title"
        mock_fields.labels = ['CVE-1999-2000']
        # Commit Hashes is customfield_12324041
        mock_fields.customfield_12324041 = 'abcd1234567890'
        mock_issue.fields = mock_fields
        mock_rhissue = mock.Mock(spec_set=['id', 'ji', 'ji_project', 'ji_branch', 'jira_link',
                                           'ji_fix_version', 'ji_cves', 'ji_commit_hashes'])
        mock_rhissue.id = mock_issue.key
        mock_rhissue.ji = mock_issue
        mock_rhissue.ji_project = mock_project
        mock_rhissue.ji_branch = mock_branch
        mock_rhissue.ji_fix_version = 'rhel-8.10'
        mock_rhissue.ji_cves = ['CVE-1984-2001']
        mock_rhissue.ji_commit_hashes = ['abcd1234567890']
        mock_rhissue.jira_link = 'https://issues.redhat.com/browse/RHEL-101'

        mock_repo().head.commit.summary = 'commit title'
        mock_repo().git.log.return_value = 'git log output'
        mock_repo().active_branch.name = 'foo'
        mock_repo().working_tree_dir = '/src/foo'

        mock_backport = libbackport.Backport(mock_rhissue, '/src/kernel-ark')

        with self.assertLogs('cki.webhook.utils.backporter', level='DEBUG') as logs:
            backporter.try_to_backport(mock_session, mock_backport)
            self.assertIn("Fetching from git remotes", logs.output[-2])
            self.assertIn("Successfully backported commits ['abcd1234567890'] for RHEL-101",
                          logs.output[-1])
            expected_msg = ('commit title\n\n'
                            'JIRA: https://issues.redhat.com/browse/RHEL-101  \n'
                            'CVE: CVE-1984-2001\n\n'
                            'git log output')
            mock_repo().git.commit.assert_called_with('--reset-author', '-s', '--amend', '-m',
                                                      expected_msg)

        mock_gcp.side_effect = CalledProcessError(2, ["bad"])
        with self.assertLogs('cki.webhook.utils.backporter', level='DEBUG') as logs:
            backporter.try_to_backport(mock_session, mock_backport)
            self.assertIn("Fetching from git remotes", logs.output[-2])
            self.assertIn("Unable to auto-backport commits for RHEL-101 (failed on abcd1234567890)",
                          logs.output[-1])

    @mock.patch('webhook.kgit.prune_remote', mock.Mock(return_value=True))
    def test_git_branch_exists(self):
        mock_backport = mock.Mock(spec_set=['repo'])
        mock_repo = mock.Mock(spec_set=['active_branch', 'working_tree_dir'])
        mock_branch = mock.Mock(spec_set=['name'])
        mock_branch.name = 'backport-RHEL-101-rhel-8-sandbox-main'
        mock_repo.active_branch = mock_branch
        mock_repo.working_tree_dir = f'/src/{mock_branch.name}'
        mock_backport.repo = mock_repo
        mock_remote = mock.Mock(spec_set=['name', 'refs'])
        mock_remote.name = 'bot-rhel-8-sandbox'
        mock_ref = mock.Mock(spec_set=['name'])
        mock_ref.name = 'bar'
        mock_remote.refs = [mock_ref]
        self.assertFalse(backporter.git_branch_exists(mock_backport, mock_remote))
        mock_ref.name = 'bot-rhel-8-sandbox/backport-RHEL-101-rhel-8-sandbox-main'
        self.assertTrue(backporter.git_branch_exists(mock_backport, mock_remote))

    def test_mr_for_branch_exists(self):
        mock_project = mock.Mock(spec_set=['name', 'namespace', 'mergerequests'])
        mock_project.name = 'rhel-8-sandbox'
        mock_project.namespace = 'redhat/rhel/kernel/src/rhel-8-sandbox'
        mock_project.mergerequests.list.return_value = []
        bp_branch = 'backport-RHEL-101-rhel-8-sandbox-main'
        target_branch = 'main'
        self.assertFalse(backporter.mr_for_branch_exists(mock_project, bp_branch, target_branch))
        mock_mr = mock.Mock(spec_set=['source_branch'])
        mock_mr.source_branch = 'backport-RHEL-101-rhel-8-sandbox-main'
        mock_project.mergerequests.list.return_value = [mock_mr]
        self.assertTrue(backporter.mr_for_branch_exists(mock_project, bp_branch, target_branch))

    @mock.patch('webhook.kgit.branches_differ')
    @mock.patch('webhook.utils.backporter.mr_for_branch_exists')
    @mock.patch('webhook.utils.backporter.git_branch_exists')
    @mock.patch('webhook.kgit.prune_remote', mock.Mock(return_value=True))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.libbackport.Repo', mock.Mock(return_value=True))
    def test_submit_merge_request(self, mock_gbe, mock_mrfbe, mock_gbd):
        mock_session = backport_session_runner()
        mock_gbe.return_value = False
        mock_mrfbe.return_value = False
        mock_gbd.return_value = True

        mock_project = mock.Mock(spec_set=['id', 'name', 'path_with_namespace', 'mergerequests'])
        mock_project.id = 1234
        mock_project.name = 'rhel-8-sandbox'
        mock_project.path_with_namespace = 'redhat/rhel/kernel/src/rhel-8-sandbox'
        mock_project.mergerequests.list = []
        mock_ji_project = mock.Mock(spec_set=['name', 'namespace'])
        mock_ji_project.name = 'rhel-8-sandbox'
        mock_ji_project.namespace = 'redhat/rhel/kernel/src/rhel-8-sandbox'
        mock_fork_proj = mock.Mock(spec_set=['name', 'path_with_namespace', 'mergerequests'])
        mock_fork_proj.name = 'rhel-8-sandbox'
        fns = 'redhat/red-hat-ci-tools/kernel/bot-branches/rhel-8-sandbox'
        mock_fork_proj.path_with_namespace = fns
        mock_session.gl_instance.projects.get.return_value = mock_fork_proj
        mock_branch = mock.Mock(spec_set=['name'])
        mock_branch.name = 'main'
        mock_issue = mock.Mock(spec_set=['key', 'fields'])
        mock_issue.key = 'RHEL-101'
        mock_fields = mock.Mock(spec_set=['summary', 'labels', 'assignee', JiraField.Commit_Hashes])
        mock_fields.summary = "jira bug title"
        mock_fields.labels = ['CVE-1999-2000']
        # Commit Hashes is customfield_12324041
        mock_fields.customfield_12324041 = 'abcd1234567890'
        mock_assignee = mock.Mock(spec_set=['name'])
        mock_assignee.name = 'cki-kwf-bot'
        mock_issue.fields = mock_fields
        mock_fields.assignee = mock_assignee
        mock_rhissue = mock.Mock(spec_set=['id', 'ji', 'ji_project', 'ji_branch', 'jira_link',
                                           'ji_fix_version', 'ji_cves', 'ji_commit_hashes'])
        mock_rhissue.id = mock_issue.key
        mock_rhissue.ji = mock_issue
        mock_rhissue.ji_project = mock_ji_project
        mock_rhissue.ji_branch = mock_branch
        mock_rhissue.ji_fix_version = 'rhel-8.10'
        mock_rhissue.ji_cves = ['CVE-1984-2001']
        mock_rhissue.ji_commit_hashes = ['abcd1234567890']
        mock_rhissue.jira_link = 'https://issues.redhat.com/browse/RHEL-101'

        mock_backport = libbackport.Backport(mock_rhissue, '/src/kernel-ark')

        mock_repo = mock.Mock(spec_set=['head', 'active_branch', 'working_tree_dir', 'remote'])
        mock_commit = mock.Mock(spec_set=['summary', 'message'])
        mock_commit.summary = 'commit title'
        mock_commit.message = 'commit title\n\ncommit body text'
        mock_repo.head.commit = mock_commit
        mock_repo.active_branch = mock.Mock(spec_set=['name'])
        mock_repo.active_branch.name = 'foo'
        mock_repo.working_tree_dir = '/src/foo'
        mock_remote = mock.Mock(spec_set=['name', 'refs', 'push'])
        mock_remote.name = f'bot-{mock_project.name}'
        mock_ref = mock.Mock(spec_set=['name'])
        mock_ref.name = f'{mock_remote.name}/{mock_repo.active_branch.name}'
        mock_remote.refs = []
        mock_repo.remote.return_value = mock_remote
        mock_backport.repo = mock_repo
        mock_mr = mock.Mock(spec_set=['web_url'])
        mock_mr.web_url = 'https://gitlab.com/namespace/project/-/merge_requests/1234'
        mock_fork_proj.mergerequests.create.return_value = mock_mr
        mock_session.get_gl_project = mock.Mock(return_value=mock_project)

        with self.assertLogs('cki.webhook.utils.backporter', level='DEBUG') as logs:
            backporter.submit_merge_request(mock_session, mock_backport)
            self.assertIn("Creating backport for RHEL-101", logs.output[-3])
            self.assertIn("Adding label kwf-backport-success", logs.output[-2])
            self.assertIn("KWF backport automation successfully created a merge request",
                          logs.output[-2])
            self.assertIn("New MR: https://gitlab.com/namespace/project/-/merge_requests/1234",
                          logs.output[-1])

        mock_mrfbe.return_value = True
        with self.assertLogs('cki.webhook.utils.backporter', level='DEBUG') as logs:
            backporter.submit_merge_request(mock_session, mock_backport)
            self.assertIn("Creating backport for RHEL-101", logs.output[-2])
            self.assertIn("Existing open MR found, not creating a new one", logs.output[-1])

        mock_remote.refs = [mock_ref]
        mock_gbe.return_value = True
        with self.assertLogs('cki.webhook.utils.backporter', level='DEBUG') as logs:
            backporter.submit_merge_request(mock_session, mock_backport)
            self.assertIn("Creating backport for RHEL-101", logs.output[-3])
            self.assertIn("Existing backport branch found in bot remote", logs.output[-2])
            self.assertIn("Updated existing branch for RHEL-101", logs.output[-1])

        mock_gbd.return_value = False
        with self.assertLogs('cki.webhook.utils.backporter', level='DEBUG') as logs:
            backporter.submit_merge_request(mock_session, mock_backport)
            self.assertIn("Creating backport for RHEL-101", logs.output[-3])
            self.assertIn("Existing backport branch found in bot remote", logs.output[-2])
            self.assertIn("Latest backport attempt does not differ from prior one", logs.output[-1])

        mock_session.args.testing = True
        with self.assertLogs('cki.webhook.utils.backporter', level='DEBUG') as logs:
            backporter.submit_merge_request(mock_session, mock_backport)
            self.assertIn("Creating backport for RHEL-101", logs.output[-2])
            self.assertIn("Not pushing in testing mode.", logs.output[-1])

    @mock.patch.dict('os.environ', {'RHKERNEL_SRC': ''})
    @mock.patch('webhook.session.connect_jira', mock.Mock())
    @mock.patch('webhook.kgit.worktree_add', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.setup_git_user', mock.Mock())
    @mock.patch('webhook.utils.backporter.submit_merge_request', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.backporter.try_to_backport', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.backporter.make_rhissues')
    @mock.patch('webhook.utils.backporter.get_linked_mrs')
    @mock.patch('webhook.session.SessionRunner')
    @mock.patch('webhook.libbackport.Repo', mock.Mock(return_value=True))
    def test_main(self, mock_session, mock_glm, mock_mji):
        mock_args = '-j RHEL-101'
        with self.assertLogs('cki.webhook.utils.backporter', level='WARNING') as logs:
            backporter.main(mock_args.split())
            self.assertIn("No path to RH Kernel source git found, aborting!", logs.output[-1])

        mock_args = '-r /foo'
        with self.assertLogs('cki.webhook.utils.backporter', level='WARNING') as logs:
            backporter.main(mock_args.split())
            self.assertIn("No jira issue specified (-j/--jira), aborting!", logs.output[-1])

        mock_project = mock.Mock(spec_set=['name', 'namespace'])
        mock_project.name = 'rhel-8-sandbox'
        mock_project.namespace = 'redhat/rhel/kernel/src/rhel-8-sandbox'
        mock_branch = mock.Mock(spec_set=['name'])
        mock_branch.name = 'main'
        mock_issue = mock.Mock(spec_set=['key', 'fields'])
        mock_issue.key = 'RHEL-101'
        mock_fields = mock.Mock(spec_set=['summary', 'labels'])
        mock_fields.summary = "jira bug title"
        mock_fields.labels = ['kwf-backport-fail']
        mock_issue.fields = mock_fields
        mock_rhissue = mock.Mock(spec_set=['id', 'ji', 'ji_project', 'ji_branch', 'ji_fix_version',
                                           'ji_affects_versions', 'ji_commit_hashes', 'jira_link'])
        mock_rhissue.id = mock_issue.key
        mock_rhissue.ji = mock_issue
        mock_rhissue.ji_project = None
        mock_rhissue.ji_branch = mock_branch
        mock_rhissue.ji_fix_version = None
        mock_rhissue.ji_affects_versions = ['rhel-8.10']
        mock_rhissue.ji_commit_hashes = []
        mock_rhissue.jira_link = 'https://issues.redhat.com/browse/RHEL-101'

        mock_mji.return_value = [mock_rhissue]

        mock_args = '-r /src/kernel-ark -j RHEL-101'.split()
        mock_session.jira.issue.return_value = mock_issue
        with self.assertLogs('cki.webhook.utils.backporter', level='INFO') as logs:
            backporter.main(mock_args)
            self.assertIn("No project found for environment, aborting.", logs.output[-1])

        mock_rhissue.ji_project = mock_project
        mock_glm.return_value = ['there', 'are', 'mrs']
        with self.assertLogs('cki.webhook.utils.backporter', level='INFO') as logs:
            backporter.main(mock_args)
            self.assertIn("Prior backport attempts already failed, aborting.", logs.output[-1])

        mock_fields.labels = ['kwf-backport-success']
        with self.assertLogs('cki.webhook.utils.backporter', level='INFO') as logs:
            backporter.main(mock_args)
            self.assertIn("Prior backport attempt was successful, no need to try again.",
                          logs.output[-1])

        mock_fields.labels = ['CVE-1999-2000']
        with self.assertLogs('cki.webhook.utils.backporter', level='INFO') as logs:
            backporter.main(mock_args)
            self.assertIn("Aborting due to RHEL-101 having no Commit Hashes specified",
                          logs.output[-1])

        mock_rhissue.ji_commit_hashes = ['abcd1234567890']
        with self.assertLogs('cki.webhook.utils.backporter', level='DEBUG') as logs:
            backporter.main(mock_args)
            self.assertIn("Got issue RHEL-101 (jira bug title), commit hashes: ['abcd1234567890']",
                          logs.output[-2])
            self.assertIn("Aborting due to existing MR in RHEL-101", logs.output[-1])

        mock_glm.return_value = []
        with self.assertLogs('cki.webhook.utils.backporter', level='INFO') as logs:
            backporter.main(mock_args)
            self.assertIn("Aborting due to no Fix Version set in RHEL-101", logs.output[-1])

        mock_rhissue.ji_fix_version = 'rhel-8.10'
        with self.assertNoLogs('cki.webhook.utils.backporter', level='INFO') as logs:
            backporter.main(mock_args)
