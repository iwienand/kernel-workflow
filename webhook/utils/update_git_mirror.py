"""Clone or update a git mirror repo."""
import argparse
import contextlib
import os
import subprocess
import sys
import time

from cki_lib import logger
from cki_lib import misc
from cki_lib import s3bucket
from cki_lib.yaml import load
import sentry_sdk

from .. import common

LOGGER = logger.get_logger('cki.webhook.utils.update_git_mirror')


class RepoManager:
    """Manage a local git mirror repo."""

    def __init__(self, args, configs):
        """Create a new repo manager instance."""
        self.repo_path = args.repo_path
        self.configs = {key: self._clean_config(configs, key)
                        for key in configs if not key.startswith('.')}

        self.bucket_spec = os.environ[args.bucket_config_name]
        self.s3_tarball_filename = args.s3_tarball_filename

    def setup(self):
        """Create a git repo manager."""
        LOGGER.info('Create empty repository at %s', self.repo_path)
        os.makedirs(self.repo_path, exist_ok=True)
        subprocess.run(['git', 'init', '.'],
                       cwd=self.repo_path, check=True)
        subprocess.run(['git', 'config', 'gc.auto', '0'],
                       cwd=self.repo_path, check=True)
        subprocess.run(['git', 'config', 'merge.directoryRenames', 'true'],
                       cwd=self.repo_path, check=True)

    def fetch_all(self, only_log_exceptions=False):
        """Fetch all remote git repos."""
        for remote_name, remote_config in self.configs.items():
            if remote_config.get('retired'):
                LOGGER.info('Removing retired remote %s from %s', remote_name, remote_config)
                self._remove_remote(remote_name)
                continue
            LOGGER.info('Updating remote %s from %s', remote_name, remote_config)
            with misc.only_log_exceptions() if only_log_exceptions else contextlib.nullcontext():
                self._fetch_repo(remote_name)

    def setup_remotes(self, only_log_exceptions=False):
        """Configure all remote git repos."""
        for remote_name, remote_config in self.configs.items():
            LOGGER.info('Configuring remote %s from %s', remote_name, remote_config)
            with misc.only_log_exceptions() if only_log_exceptions else contextlib.nullcontext():
                self._ensure_remote(remote_name, remote_config)

    def checkout(self, only_log_exceptions=False):
        """Reset the checkout to the first remote with checkout!=null."""
        for remote_name, remote_config in self.configs.items():
            if not remote_config['checkout']:
                continue
            LOGGER.info('Resetting checkout to %s/%s', remote_name, remote_config['checkout'])
            with misc.only_log_exceptions() if only_log_exceptions else contextlib.nullcontext():
                self._git(['reset', '--hard', f'{remote_name}/{remote_config["checkout"]}'])
            break

    def garbage_collection(self, force=False):
        """Run git garbage collection and repack."""
        LOGGER.info('Running git garbage collection')
        self._git(['gc'] + ([] if force else ['--auto']))
        self._git(['repack', '--geometric', '2', '-d'])

    def setup_git_user(self):
        """Set git user.name and user.email so we can merge/commit in this repo."""
        self._git(['config', 'user.email', 'cki-kwf-bot@redhat.com'])
        self._git(['config', 'user.name', 'CKI KWF BOT'])

    def pull_s3(self):
        """Pull a tarball of the repo from an S3 bucket."""
        LOGGER.info('Pulling from S3 and extracting to %s', self.repo_path)
        aws_kwargs = self._aws_s3_kwargs('pull')
        tar_args = ['tar', '--extract', '--file', '-',
                    '--no-same-owner', '--no-same-permissions', '--no-overwrite-dir']
        os.makedirs(self.repo_path, exist_ok=True)
        with subprocess.Popen(stdout=subprocess.PIPE, **aws_kwargs) as aws, \
                subprocess.Popen(tar_args, stdin=aws.stdout, cwd=self.repo_path) as tar:
            aws.stdout.close()  # Allow tar to receive a SIGPIPE if aws exits
        self._check_process([aws, tar])

    def push_s3(self):
        """Push a tarball of the repo to an S3 bucket."""
        LOGGER.info('Pushing from %s to S3', self.repo_path)
        tar_args = ['tar', '--create', '--file', '-', '.']
        aws_kwargs = self._aws_s3_kwargs('push')
        with subprocess.Popen(tar_args, stdout=subprocess.PIPE, cwd=self.repo_path) as tar, \
                subprocess.Popen(stdin=tar.stdout, **aws_kwargs) as aws:
            tar.stdout.close()  # Allow aws to receive a SIGPIPE if tar exits
        self._check_process([tar, aws])

    @staticmethod
    def _clean_config(configs, key):
        """Return a clean version of one individual configuration."""
        config = configs[key]
        if isinstance(config, str):
            config = {'url': config}
        return {**configs['.default'], **config}

    def _git(self, args, **kwargs):
        """Run the git executable in the local repo directory."""
        kwargs.setdefault('check', True)
        # pylint: disable=subprocess-run-check
        return subprocess.run(['git'] + args, cwd=self.repo_path, **kwargs)

    @staticmethod
    def _check_process(processes):
        """Check all passed processes for non-zero return codes."""
        for process in processes:
            retcode = process.poll()
            if retcode:
                raise subprocess.CalledProcessError(retcode, process.args)

    def _aws_s3_kwargs(self, push_or_pull):
        """Return the arguments for popen for an aws s3 CLI invocation."""
        parsed_spec = s3bucket.parse_bucket_spec(self.bucket_spec)
        s3_url = f's3://{parsed_spec.bucket}/{parsed_spec.prefix}{self.s3_tarball_filename}'
        kwargs = {}
        kwargs['args'] = ['aws', 's3', 'cp']
        kwargs['args'] += [s3_url, '-'] if push_or_pull == 'pull' else ['-', s3_url]
        if parsed_spec.endpoint:
            kwargs['args'] += ['--endpoint', parsed_spec.endpoint]
        if parsed_spec.access_key:
            kwargs['env'] = dict(os.environ,
                                 AWS_ACCESS_KEY_ID=parsed_spec.access_key,
                                 AWS_SECRET_ACCESS_KEY=parsed_spec.secret_key)
        return kwargs

    def _fetch_repo(self, remote_name):
        """Fetch a single git repo."""
        try:
            self._git(['fetch', remote_name])
        except subprocess.CalledProcessError as err:
            LOGGER.warning('Fetch of %s failed: %s', remote_name, err)

    def _remove_remote(self, remote_name):
        """Remove a retired/stale/archived remote."""
        remotes = self._git(['remote'], capture_output=True, encoding='utf8').stdout.splitlines()
        if remote_name in remotes:
            self._git(['remote', 'remove', remote_name])

    def _ensure_remote(self, remote_name, remote_config):
        """Ensure a remote is registered in the the git repo."""
        remotes = self._git(['remote'], capture_output=True, encoding='utf8').stdout.splitlines()
        git_url = remote_config['url']
        if remote_name not in remotes:
            tagopt = '--tags' if remote_config['fetch-tags'] else '--no-tags'
            self._git(['remote', 'add', remote_name, git_url, tagopt])
            for ref_spec in remote_config['ref-specs']:
                self._git(['config', '--add', f'remote.{remote_name}.fetch',
                           ref_spec.format(name=remote_name)])
        else:
            self._git(['remote', 'set-url', remote_name, git_url])
        if remote_config['token']:
            self._git(['config', '--global', f'credential.{git_url}.username', 'oauth2'])
            self._git(['config', '--global', f'credential.{git_url}.helper',
                       f'!echo password=${remote_config["token"]}; :'])


def main(args):
    """Run main loop."""
    parser = argparse.ArgumentParser(description='Clone or update the upstream git repo')
    parser.add_argument('--sentry-ca-certs', default=os.getenv('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    parser.add_argument('--update-interval-minutes', type=int,
                        help='Continuously update with the given interval and ignore exceptions')
    parser.add_argument('--repo-path', **common.get_argparse_environ_opts('REPO_PATH'),
                        help='Local path where the git mirror repo is stored')
    mirror_list_group = parser.add_mutually_exclusive_group()
    mirror_list_group.add_argument('--mirror-list', default=os.getenv('MIRROR_LIST'),
                                   help='Yaml string with list of git repos to mirror')
    mirror_list_group.add_argument('--mirror-list-path', default=os.getenv('MIRROR_LIST_PATH'),
                                   help='Yaml file with list of git repos to mirror')
    parser.add_argument('--bucket-config-name',
                        **common.get_argparse_environ_opts('BUCKET_CONFIG_NAME'),
                        help='Name of the environment variable with the S3 cache bucket spec')
    parser.add_argument('--s3-tarball-filename',
                        **common.get_argparse_environ_opts('S3_TARBALL_FILENAME'),
                        help='Tarball file name on S3')
    parser.add_argument('--force-gc', action='store_true', default=False,
                        help='Do not use --auto when running garbage collection')
    parser.add_argument('--setup-git-user', action='store_true', default=False,
                        help="Set up git user.name and user.info to allow commit/merge in repo")
    parser.add_argument('--mode', choices=('create-s3', 'update-s3', 'create-repo', 'update-repo'),
                        required=True, help='create empty S3 cache (create-s3), '
                                            'update the S3 cache (update-s3), '
                                            'create a local repo from the S3 cache (create-repo), '
                                            'or update a local repo (update-repo)')
    parsed_args = parser.parse_args(args)

    misc.sentry_init(sentry_sdk, ca_certs=parsed_args.sentry_ca_certs)

    if not (configs := load(contents=parsed_args.mirror_list,
            file_path=parsed_args.mirror_list_path)):
        LOGGER.warning('One of the arguments --mirror-list or --mirror-list-path is required')
        sys.exit(1)

    repo_manager = RepoManager(parsed_args, configs)

    while True:
        if parsed_args.mode == 'create-s3':
            LOGGER.info('Create empty git mirror repo and push to S3')
            repo_manager.setup()
            repo_manager.push_s3()
        if parsed_args.mode == 'update-s3':
            LOGGER.info('Updating git mirror on S3')
            repo_manager.pull_s3()
            repo_manager.setup_remotes()
            repo_manager.fetch_all()
            repo_manager.garbage_collection(force=parsed_args.force_gc)
            repo_manager.checkout()
            repo_manager.push_s3()
        if parsed_args.mode == 'create-repo':
            LOGGER.info('Populating local git mirror repo from S3')
            repo_manager.pull_s3()
            repo_manager.setup_remotes()
            if parsed_args.setup_git_user:
                repo_manager.setup_git_user()
        elif parsed_args.mode == 'update-repo':
            LOGGER.info('Updating local git mirror repo')
            repo_manager.fetch_all(only_log_exceptions=bool(parsed_args.update_interval_minutes))
            repo_manager.checkout(only_log_exceptions=bool(parsed_args.update_interval_minutes))

        if not parsed_args.update_interval_minutes:
            break

        LOGGER.info('Sleeping for %s minutes', parsed_args.update_interval_minutes)
        time.sleep(60 * parsed_args.update_interval_minutes)


if __name__ == "__main__":
    main(sys.argv[1:])
