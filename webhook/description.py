"""Library to consistently parse an MR or commit description."""
from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
import re
import typing

from cki_lib.logger import get_logger
from unidecode import unidecode

from webhook.defs import DCOState
from webhook.defs import JPFX
from webhook.graphql import GitlabGraph
from webhook.users import User

LOGGER = get_logger('cki.webhook.description')


@dataclass
class Description:
    """Parse commit description text tags into sets of IDs."""

    BUGZILLA_TAG = 'Bugzilla'
    JIRA_ISSUE_TAG = 'JIRA'

    text: str = field(default='', repr=False)

    def __post_init__(self):
        """Deal with None for text string intput."""
        if self.text is None:
            self.text = ''

    def __bool__(self):
        """Return True if self.text is not empty, otherwise False."""
        return bool(self.text)

    def __eq__(self, other):
        """Return True if the text is the same."""
        return self.text == other.text

    @staticmethod
    def _parse_tag(tag_prefix, text):
        """Return the set of tag IDs as ints."""
        # tag_prefix: http://bugzilla.redhat.com/1234567
        # tag_prefix: https://bugzilla.redhat.com/show_bug.cgi?id=1234567
        pattern = r'^' + tag_prefix + \
            r': https?://bugzilla\.redhat\.com/(?:show_bug\.cgi\?id=)?(\d{4,8})\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return {int(tag) for tag in tag_regex.findall(text)}

    @staticmethod
    def _parse_jira_tag(tag_prefix, text):
        """Return the set of tags as strs."""
        # tag_prefix: https://issues.redhat.com/browse/RHEL-1
        # tag_prefix: https://issues.redhat.com/projects/RHEL/issues/RHEL-1
        pattern = r'^' + tag_prefix + \
            r': https://issues\.redhat\.com/(?:browse|projects/RHEL/issues)/(' + \
            JPFX + r'\d{1,8})\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return {str(tag) for tag in tag_regex.findall(text)}

    @property
    def bugzilla(self):
        """Return the set of BZ IDs parsed from any Bugzilla: tags."""
        return self._parse_tag(self.BUGZILLA_TAG, self.text)

    @property
    def jira_tags(self):
        """Return the set of Jira Issue IDs parsed from any JIRA: tags."""
        return self._parse_jira_tag(self.JIRA_ISSUE_TAG, self.text)

    @property
    def cve(self):
        """Return the set of CVE IDs parsed from CVE: tags."""
        pattern = r'^CVE: (CVE-\d{4}-\d{4,7})\s*$'
        cve_regex = re.compile(pattern, re.MULTILINE)
        return set(cve_regex.findall(self.text))

    @property
    def marked_internal(self):
        """Return True if the text has Bugzilla|JIRA: INTERNAL, otherwise False."""
        pattern = r'^(' + self.BUGZILLA_TAG + '|' + self.JIRA_ISSUE_TAG + r'): INTERNAL\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return bool(tag_regex.findall(self.text))

    @property
    def signoff(self):
        """Return the set of valid DCO Signed-off-by tags."""
        # Each item in the set is a tuple with the name and email.
        # There is no validation of the email string.
        pattern = r'^Signed-off-by: (.+) <(.+)>\s*$'
        dco_regex = re.compile(pattern, re.MULTILINE)
        return set(dco_regex.findall(self.text))


@dataclass
class MRDescription(Description):
    """Parse MR description text tags into sets of IDs."""

    DEPENDS_TAG = 'Depends'

    namespace: str = ''
    graphql: GitlabGraph | None = None
    depends_bzs: set = field(default_factory=set, init=False)  # Resolved depends BZ IDs.
    depends_jiras: set = field(default_factory=set, init=False)  # Resolved depends Jira IDs.
    _depends_mrs: set = field(default_factory=set, init=False)  # Resolved depends MR IDs.

    def __post_init__(self):
        """Resolve the dependencies, if given a graphql object."""
        if not self.namespace:
            return
        # Troll through the depends_mrs and stash their Jira/BZ IDs in _depends.
        depends_mr_descriptions = self._depends_mr_descriptions()
        self._depends_mrs.update(depends_mr_descriptions.keys())
        self.depends_bzs.update(bz_id for desc in depends_mr_descriptions.values() for
                                bz_id in desc.bugzilla | desc.depends_bzs)
        self.depends_jiras.update(jira_id for desc in depends_mr_descriptions.values() for
                                  jira_id in desc.jira_tags | desc.depends_jiras)

    def __eq__(self, other):
        """Return True if the text, namespace, and _depends attribute values are the same."""
        return self.text == other.text and self.namespace == other.namespace and \
            self.depends_bzs == other.depends_bzs and self.depends_jiras == other.depends_jiras

    @property
    def _base_depends_mrs(self):
        """Return the set of MR IDs parsed from any Depends: tags."""
        # Depends: https://gitlab.com/group/subgroup/project/-/merge_requests/123
        # Depends: !123
        dep_mrs = set()
        if self.namespace:
            pattern = r'^' + self.DEPENDS_TAG + \
                r': (?:https?://gitlab\.com/' + self.namespace + r'/-/merge_requests/|!)(\d+)\s*$'
        else:
            pattern = r'^' + self.DEPENDS_TAG + r': !(\d+)\s*$'
        mr_regex = re.compile(pattern, re.MULTILINE)
        dep_mrs.update(int(mr_id) for mr_id in mr_regex.findall(self.text))
        return dep_mrs

    @property
    def cc(self):
        # pylint: disable=invalid-name
        """Return the set of Cc name/email tuples from the Cc: tags."""
        # Cc: Example User <example_user@example.com>
        # Cc: <email_only@example.com>
        pattern = r'^Cc:(?:| (.*)) <(.*)>\s*$'
        cc_regex = re.compile(pattern, re.MULTILINE)
        return set(cc_regex.findall(self.text))

    @property
    def depends(self):
        """Return all the Depends BZs and Jiras."""
        return self.depends_bzs | self.depends_jiras

    @property
    def depends_mrs(self):
        """Return base_depends_mrs plus _depends_mrs."""
        return self._base_depends_mrs | self._depends_mrs

    def _depends_mr_descriptions(self):
        """Return a dict of all the depends_mrs descriptions."""
        if not self.namespace or not self.graphql:
            return {}
        mr_descriptions = {}
        todo_mrs = self.depends_mrs
        max_level = 5
        while max_level and todo_mrs:
            max_level -= 1
            raw_desc_dict = self.graphql.get_mr_descriptions(self.namespace, todo_mrs)
            new_descriptions = {iid: MRDescription(raw_desc, self.namespace, self.graphql) for
                                iid, raw_desc in raw_desc_dict.items()}
            todo_mrs = {iid for desc in new_descriptions.values() for iid in desc.depends_mrs if
                        iid not in mr_descriptions}
            mr_descriptions.update(new_descriptions)
        return mr_descriptions


Signoff = typing.Tuple[str, str]


@dataclass(kw_only=True)
class Commit:
    # pylint: disable=too-many-instance-attributes
    """Properties of a commit."""

    author: User | None = field(default=None)
    author_email: str = ''
    author_name: str = ''
    committer_email: str = ''
    committer_name: str = ''
    date: datetime | None = field(default=None)
    description: Description | MRDescription = field(default_factory=Description)
    sha: str = ''
    title: str = ''
    input_dict: InitVar[dict] = {}
    zstream: bool = False

    def __post_init__(self, input_dict):
        """Set it up from the input_dict."""
        if input_dict:
            if author_dict := input_dict.get('author'):
                if self.author:
                    raise ValueError('input_dict has author key but author attrib is already set.')
                self.author = User(user_dict=author_dict)
            self.author_email = input_dict.get('authorEmail', '')
            self.author_name = input_dict.get('authorName', '')
            self.committer_email = input_dict.get('committerEmail', '')
            self.committer_name = input_dict.get('committerName', '')
            self.date = datetime.fromisoformat(input_dict['authoredDate'][:19]) if \
                'authoredDate' in input_dict else None
            self.description = Description(text=input_dict.get('description', ''))
            self.sha = input_dict.get('sha', '')
            self.title = input_dict.get('title', '')
        # If GL has associated this commit with this User then we can assume the commit
        # authorEmail belongs to this User.
        if self.author and self.author_email:
            self.author.add_email(self.author_email)
        LOGGER.debug('Created %s', self)

    def __repr__(self):
        """Display it."""
        repr_str = f"{self.short_sha} ('{self.title}')"
        author = self.author or f'{self.author_name} ({self.author_email})'
        committer = f'{self.committer_name} ({self.committer_email})'
        repr_str += f', author: {author}, committer: {committer}, dco: {self.dco_state.name}'
        return f'<Commit {repr_str}>'

    @property
    def dco_state(self):
        """Return the DCOState of the Commit."""
        # There were no signoffs found in the commit description.
        if not self.description.signoff or not self.expected_signoff:
            return DCOState.MISSING
        # unidecode the expected signoff.
        # https://github.com/pylint-dev/pylint/issues/1498
        # pylint: disable=unsubscriptable-object
        expected_signoff = (unidecode(self.expected_signoff[0]), self.expected_signoff[1])
        # At least one of the expected signoffs matches a Description.signoff.
        description_signoffs = [(unidecode(name), mail) for name, mail in self.description.signoff]
        if expected_signoff in description_signoffs:
            # OK if there is an @redhat.com signoff, otherwise NOT_REDHAT.
            return DCOState.OK if expected_signoff[1].endswith('@redhat.com') \
                else DCOState.OK_NOT_REDHAT
        state = DCOState.UNRECOGNIZED
        # Do any of the commit DCOs have a name that matches the commit commiter name?
        if any(dco for dco in description_signoffs if dco[0] == expected_signoff[0]):
            state = DCOState.NAME_MATCHES
        # ... or do any of the commit DCOs have an email address that matches the committer email?
        elif any(dco for dco in description_signoffs if dco[1] == expected_signoff[1]):
            state = DCOState.EMAIL_MATCHES
        return state

    @property
    def expected_author_signoff(self) -> typing.Union[Signoff, None]:
        """Return the expected signoff for the author."""
        return (self.author_name, self.author_email) if \
            self.author_name and self.author_email else None

    @property
    def expected_committer_signoff(self) -> typing.Union[Signoff, None]:
        """Return the expected signoff for the committer."""
        return (self.committer_name, self.committer_email) if \
            self.committer_name and self.committer_email else None

    @property
    def expected_signoff(self) -> typing.Union[Signoff, None]:
        """Return a tuple with the expected DCO signoff depending on self.zstream, or None."""
        return self.expected_committer_signoff if self.zstream else self.expected_author_signoff

    @property
    def short_sha(self):
        """Return the first 12 chars of the sha."""
        return self.sha[:12]
