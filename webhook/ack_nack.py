"""Process all of the Approvals that are associated with a merge request."""
from dataclasses import dataclass
from functools import cached_property
import sys
import typing

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
from gitlab.const import MAINTAINER_ACCESS
import prometheus_client as prometheus

from . import cdlib
from . import common
from . import defs
from .base_mr import BaseMR
from .base_mr_mixins import ApprovalsMixin
from .base_mr_mixins import CommitsMixin
from .base_mr_mixins import DependsMixin
from .base_mr_mixins import DiffsMixin
from .base_mr_mixins import OwnersMixin
from .description import MRDescription
from .session import SessionRunner

if typing.TYPE_CHECKING:
    from cki_lib.owners import Parser
    from gitlab.client import Gitlab
    from gitlab.v4.objects.merge_requests import ProjectApprovalRule
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest

    from .approval_rules import ApprovalRule
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.ack_nack')

APPROVAL_RULE_ACKS = (' - Approval Rule "%s" requires at least %d ACK(s) (%d given) '
                      'from set (%s).  \n')
APPROVAL_RULE_OKAY = ' - Approval Rule "%s" already has %d ACK(s) (%d required).  \n'
APPROVAL_RULE_OPT = ' - Approval Rule "%s" requests optional ACK(s) from set (%s).  \n'
STALE_APPROVALS_SUMMARY = ('Note: Approvals from %s have been invalidated due to code changes in '
                           'this merge request. Please re-approve, if appropriate.')

CC_TAGGED_USERS_RULE_NAME = 'Cc tagged users'
MISSING_CC_EMAILS_NAME = 'MISSING'

METRIC_KWF_NONPROJECT_MEMBER = prometheus.Counter(
    'kwf_nonproject_member', 'Gitlab user not a member of the project',
    ['project_name', 'username']
)


def _save(
    session: 'SessionRunner',
    approve_mr: 'ApproveMR',
    status: str,
    labels_to_add: typing.List[defs.Label],
    message: str
) -> None:
    # pylint: disable=too-many-arguments
    acks_label = f'Acks::{status}'
    note = f'**ACK/NACK Summary:** ~"{acks_label}"\n\n{message}'
    LOGGER.info(note)

    approve_mr.add_labels([acks_label] + labels_to_add)

    if not common.mr_is_closed(approve_mr.gl_mr):
        session.update_webhook_comment(approve_mr.gl_mr, note,
                                       bot_name=approve_mr.session.gl_user.username,
                                       identifier='**ACK/NACK Summary:')


def _edit_approval_rule(
    approve_mr: 'ApproveMR',
    subsystem: str,
    reviewers: set[str],
    num_req: int
) -> bool:
    """Add a GitLab Approval Rule named subsystem, with num_req required reviewers."""
    for name in approve_mr.approval_rules.keys():
        if name == subsystem:
            LOGGER.info("Approval rule for subsystem %s already exists", subsystem)
            return False

    LOGGER.info("Create rule for ss %s, with %d required approval(s) from user(s) %s",
                subsystem, num_req, list(reviewers))

    if misc.is_production_or_staging() and approve_mr.gl_mr.state == 'opened':
        ar_data = {
            "name": subsystem,
            "approvals_required": num_req,
            "usernames": list(reviewers)
        }
        approve_mr.gl_mr.approval_rules.create(data=ar_data)

    return True


def _get_reviewers(
    approve_mr: 'ApproveMR',
    changed_files: list[str],
    owners_parser: 'Parser'
) -> tuple[list[tuple], list[tuple]]:
    """Parse the owners.yaml file and return a set of email addresses and subsystem labels."""
    # pylint: disable=too-many-branches,too-many-locals,too-many-arguments,too-many-statements
    if not changed_files:
        return [], []

    opt_reviewers = {}
    req_reviewers = {}
    all_reviewers = {}
    ar_num_req = 0
    notify = False
    # A merge request can span multiple subsystems so get the reviewers for each subsystem.
    # Call owners parser individually for each file.
    for changed_file in changed_files:
        for subsystem in owners_parser.get_matching_subsystems([changed_file]):
            ss_label = subsystem.subsystem_label
            required_approvals = subsystem.required_approvals
            # Get all maintainers and reviewers
            for user in subsystem.maintainers + subsystem.reviewers:
                if not user:
                    continue
                # Ensure this user has a gitlab user name listed for it
                if 'gluser' not in user:
                    LOGGER.warning("User %s in subsystem %s does not have a gitlab username listed",
                                   user['name'], ss_label)
                    continue
                username = user['gluser']
                # Ensure user isn't listed in owners.yaml w/an unknown gitlab user name
                if username is None:
                    continue
                # Ensure that the merge request submitter doesn't show up in the reviewers list
                if approve_mr.gl_mr.author['username'] == username:
                    continue

                if ss_label not in opt_reviewers:
                    opt_reviewers[ss_label] = set([])
                    if required_approvals:
                        req_reviewers[ss_label] = set([])

                if ss_label not in all_reviewers:
                    all_reviewers[ss_label] = set([])

                all_reviewers[ss_label].update([username])

                # Ensure that restricted users aren't listed as required reviewers
                if required_approvals and not user.get('restricted', False):
                    req_reviewers[ss_label].update([username])
                else:
                    opt_reviewers[ss_label].update([username])

    # Build a rule out of any Cc: entries in the MR Description.
    cc_reviewers_usernames, missing_emails = get_cc_reviewers(approve_mr)
    if cc_reviewers_usernames:
        opt_reviewers[CC_TAGGED_USERS_RULE_NAME] = cc_reviewers_usernames

    LOGGER.debug('Subsystem optional reviewers: %s', opt_reviewers)
    LOGGER.debug('Subsystem required reviewers: %s', req_reviewers)

    notifications = []
    for subsystem, reviewers in opt_reviewers.items():
        if reviewers:
            notify = _edit_approval_rule(approve_mr, subsystem, reviewers, ar_num_req)
        if notify:
            notifications.append((subsystem, ar_num_req, reviewers))
            # If get_cc_reviewers above could not map all emails, wedge the missing list into the
            # notifications.
            if missing_emails:
                notifications.append((MISSING_CC_EMAILS_NAME, None, missing_emails))
                missing_emails = []
            notify = False

    required = []
    ar_num_req = 1
    if req_reviewers:
        amappreq = next((r.required
                         for n, r in approve_mr.approval_rules.items()
                         if n == defs.ALL_MEMBERS_APPROVAL_RULE), 1)
        authal = common.get_authlevel(approve_mr.gl_project, approve_mr.gl_mr.author['id'])
        tbranch = approve_mr.target_branch
        LOGGER.debug("access level = %d, target branch = %s, approvals required = %d",
                     authal, tbranch, amappreq)
        if authal >= MAINTAINER_ACCESS and tbranch != "main" and amappreq == 0:
            ar_num_req = 0

    for subsystem, reviewers in req_reviewers.items():
        required.append((reviewers, subsystem))
        if reviewers:
            notify = _edit_approval_rule(approve_mr, subsystem, reviewers, ar_num_req)
        if notify:
            notifications.append((subsystem, ar_num_req, reviewers))
            notify = False

    LOGGER.debug("Required reviewers: %s, notifications:\n%s", required, notifications)
    return required, notifications


def get_cc_reviewers(approve_mr: 'ApproveMR') -> tuple[set, list[str]]:
    """Return the set of usernames derived Cc: tag emails, as well as a list of missing emails."""
    cc_usernames = set()
    missing = []
    if cc_emails := {cc[1] for cc in MRDescription(approve_mr.gl_mr.description).cc}:
        cc_usernames, missing = _emails_to_gl_user_names(approve_mr.session.gl_instance, cc_emails)
        if cc_usernames:
            LOGGER.info('Mapped %s Cc email addresses to %s usernames: %s', len(cc_emails),
                        len(cc_usernames), cc_usernames)
        if missing:
            LOGGER.info('Cc email addresses which could not be mapped to usernames: %s', missing)
    return cc_usernames, missing


def _get_old_subsystems_from_labels(approve_mr: 'ApproveMR') -> tuple[list[str], list[str]]:
    subsys_list = []
    subsys_labels = []
    for label in approve_mr.labels:
        if label.startswith('Acks::'):
            sslabel_parts = label.split("::")
            if len(sslabel_parts) == 3:
                subsys_list.append(sslabel_parts[1])
                subsys_labels.append(label)
    return subsys_list, subsys_labels


def _get_stale_labels(
    old_subsystems: list[str],
    old_labels: list[str],
    subsys_scoped_labels: dict
) -> list[str]:
    stale_labels = []
    for subsystem in old_subsystems:
        if subsystem not in subsys_scoped_labels.keys():
            for label in old_labels:
                if label.startswith(f"Acks::{subsystem}::"):
                    stale_labels.append(label)
    LOGGER.debug("Stale labels: %s", stale_labels)
    return stale_labels


def _get_subsys_scoped_labels(
    approve_mr: 'ApproveMR',
    req_reviewers: list[tuple]
) -> list[str]:
    # pylint: disable=too-many-locals,too-many-arguments
    subsys_scoped_labels = {}
    (old_subsystems, old_labels) = _get_old_subsystems_from_labels(approve_mr)

    if req_reviewers is not None:
        for _, subsystem_label in req_reviewers:
            LOGGER.info("Examining label %s", subsystem_label)
            subsystem_approved = False
            for name, rule in approve_mr.approval_rules.items():
                if name == subsystem_label:
                    LOGGER.info("Getting approval status for %s", name)
                    subsystem_approved = rule.approved
                    break

            if not subsystem_approved:
                scoped_label = defs.NEEDS_REVIEW_SUFFIX
            else:
                scoped_label = defs.READY_SUFFIX

            if subsystem_label:
                subsys_scoped_labels[subsystem_label] = scoped_label

    stale_labels = _get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels)
    if stale_labels:
        approve_mr.remove_labels(stale_labels)

    ret = [f'Acks::{x}::{y}' for x, y in subsys_scoped_labels.items()]
    ret.sort()  # Sort the subsystem scoped labels for the tests.

    return ret


def _get_ar_approvals(
    approve_mr: 'ApproveMR',
    ar_reviewers: list[tuple],
    summary: str,
    faked_reset: bool
) -> tuple[str, int]:
    # pylint: disable=too-many-locals,too-many-branches
    needed = []
    optional = []
    provided = []

    if not faked_reset:
        current_approvals = approve_mr.approved_by

        if current_approvals and len(current_approvals) > 0:
            summary.append("Approved by:\n")
            for approver in current_approvals:
                summary.append(f" - {approver.name} ({approver.username})\n")
        else:
            summary.append("\n")

    req_approvals = 0
    for subsystem, required, given, usernames, _ in ar_reviewers:
        if faked_reset:
            given = 0
        reviewers = list(usernames)
        reviewers.sort()
        reviewers = ', '.join(reviewers)
        if required > 0:
            if given >= required:
                provided.append(APPROVAL_RULE_OKAY % (subsystem, given, required))
            else:
                req_approvals += required - given
                needed.append(APPROVAL_RULE_ACKS % (subsystem, required, given, reviewers))
        elif given == 0:
            optional.append(APPROVAL_RULE_OPT % (subsystem, reviewers))
        else:
            provided.append(APPROVAL_RULE_OKAY % (subsystem, given, required))

    if needed:
        summary.append("\nRequired Approvals:  \n")
        summary += needed
    if optional:
        summary.append("\nOptional Approvals:  \n")
        summary += optional
    if provided:
        summary.append("\nSatisfied Approvals:  \n")
        summary += provided

    return summary, req_approvals


def _has_unsatisfied_blocking_approval_rule(approve_mr: 'ApproveMR') -> bool:
    for name, rule in approve_mr.approval_rules.items():
        block_rule = name.startswith(defs.BLOCKED_BY_PREFIX)
        satisfied = rule.approved
        if block_rule and not satisfied:
            LOGGER.info("Rule: '%s' is not satisfied, set Acks::%s", name, defs.BLOCKED_SUFFIX)
            return True

    return False


def _parse_approval_rules(approval_rules: 'dict[ApprovalRule]') -> tuple[int, int]:
    base_req = 0
    base_left = 0
    needed = 0
    left = 0
    for name, rule in approval_rules.items():
        if name == defs.ALL_MEMBERS_APPROVAL_RULE:
            base_req = rule.required
            base_left = max(base_req - len(rule.approved_by), 0)
            continue
        needed += rule.required
        left += max(rule.required - len(rule.approved_by), 0)

    needed = max(needed, base_req)
    left = max(left, base_left)

    return needed, left


def _get_approval_summary(
    approve_mr: 'ApproveMR',
    req_reviewers: list[tuple],
    ar_reviewers: list[tuple],
    faked_reset: bool
) -> tuple[str, list[str], str]:
    summary = []
    gl_mergerequest = approve_mr.gl_mr
    labels = _get_subsys_scoped_labels(approve_mr, req_reviewers)

    summary, req_approvals = _get_ar_approvals(approve_mr, ar_reviewers, summary, faked_reset)

    if approve_mr.gl_project.only_allow_merge_if_all_discussions_are_resolved and \
       not gl_mergerequest.changes()['blocking_discussions_resolved']:
        summary.append('\nAll discussions must be resolved.')
        return defs.BLOCKED_SUFFIX, labels, ''.join(summary)

    if req_approvals > 0:
        if _has_unsatisfied_blocking_approval_rule(approve_mr):
            return defs.BLOCKED_SUFFIX, labels, ''.join(summary)

        summary.append(f'\nRequires {req_approvals} more Approval Rule Approval(s).')
        return defs.NEEDS_REVIEW_SUFFIX, labels, ''.join(summary)

    _, left = _parse_approval_rules(approve_mr.approval_rules)
    if approve_mr.has_unapproved_bot_rule():
        left -= 1
    if left:
        summary.append(f'\nRequires {left} more Approval(s).')
        return defs.NEEDS_REVIEW_SUFFIX, labels, ''.join(summary)

    summary.append('\nMerge Request has all necessary Approvals.')
    LOGGER.info("Summary is:\n%s", summary)
    return defs.READY_SUFFIX, labels, ''.join(summary)


def _emails_to_gl_user_names(
    gl_instance: 'Gitlab',
    emails: list[str]
) -> tuple[set, list[str]]:
    """Return a tuple with the mapped usernames set and list of emails that did not map."""
    users = set()
    missing = []
    for email in emails:
        if "@redhat.com" not in email and "@fedoraproject.org" not in email:
            continue
        if found_usernames := [x.username for x in gl_instance.users.list(search=email)]:
            users.update(found_usernames)
            continue
        missing.append(email)
    return users, missing


def _assign_reviewers(gl_mergerequest: 'ProjectMergeRequest', user_list: list[str]) -> None:
    """Use a quickaction to assign the given user as a reviewer of the given MR."""
    # The command requires an @ in front of the username.
    users = [user if user.startswith('@') else f'@{user}' for user in user_list]
    LOGGER.info('Assigning reviewers %s to MR %s', users, gl_mergerequest.iid)
    if misc.is_production_or_staging():
        cmd = f'/assign_reviewer {" ".join(users)}'
        gl_mergerequest.notes.create({'body': cmd})


def get_reviewers_from_approval_rules(approve_mr: 'ApproveMR') -> list[list]:
    """Read the MR's approval rules for custom-added required reviewers."""
    current_approvals = approve_mr.approved_by

    ar_reviewers = []
    for name, rule in approve_mr.approval_rules.items():
        # skip our default All Members and Ready For Merge rules
        if name in (defs.ALL_MEMBERS_APPROVAL_RULE, defs.BOT_APPROVAL_RULE):
            continue
        possible = rule.eligible
        LOGGER.debug("Rule: %s, Eligible: %s (required: %d)",
                     name, possible, rule.required)
        LOGGER.debug("Current approvals: %s", current_approvals)
        req = rule.required
        given = 0
        for approver in current_approvals:
            for eligible in possible:
                if approver.id == eligible.id:
                    given += 1
                    break
        approvers = []
        ids = []
        for approver in possible:
            approvers.append(approver.username)
            ids.append(approver.id)
        LOGGER.debug("Rule: %s, requires %s more approval(s) from set (%s )", name, req, approvers)
        ar_reviewers.append([name, req, given, approvers, ids])

    return ar_reviewers


def reset_merge_request_approvals(
    session: 'SessionRunner',
    approve_mr: 'ApproveMR',
    interdiff: str
) -> bool:
    """Post a comment about approvals being reset and optionally reset approvals on the MR.

    If the project is not configured to auto reset approvals on push then this function uses
    an API call to do it.
    Return True if the project is configured to automatically reset approvals on push.
    Return False if the project is not configured to automatically reset approvals on push.
    """
    namespace = approve_mr.namespace
    gl_project = approve_mr.gl_project
    # If the project does not automatically reset approvals on push then assume we have
    # a private token and use it to get a new gl_instance and gl_project.
    proj_reset = gl_project.approvals.get().reset_approvals_on_push
    LOGGER.info('reset_approvals_on_push: %s', proj_reset)
    if not proj_reset:
        gl_instance = get_instance(url=f'{defs.GITFORGE}/{namespace}',
                                   env_name='GITLAB_KWF_BOT_API_TOKENS')
        gl_instance.auth()
        gl_project = gl_instance.projects.get(gl_project.id)
        if not gl_instance.private_token:
            raise ValueError(f'Project {namespace} approval resets disabled, but no token!')

    gl_mergerequest = gl_project.mergerequests.get(approve_mr.iid)
    revision = len(approve_mr.diffs)
    LOGGER.info("Clearing Approvals on %s MR %s (Rev v%s)", namespace, approve_mr.iid, revision)

    # Assign people who have approved the MR whose approvals we're about to clear as Reviewers
    cleared_approvers = []
    for approver in approve_mr.approved_by:
        if approver.username in defs.BOT_ACCOUNTS:
            continue
        cleared_approvers.append(f"@{approver.username}")

    notification = ''
    if cleared_approvers:
        notification += (f'Approval(s) from {" ".join(cleared_approvers)} removed due to code '
                         f'changes, fresh approvals required on v{revision}.\n\n')
        _assign_reviewers(gl_mergerequest, cleared_approvers)

    if interdiff:
        header = f'Code changes in revision v{revision} of this MR:  \n'
        notification += common.wrap_comment_table(header, interdiff, "", f'v{revision} interdiff')

    session.update_webhook_comment(gl_mergerequest, notification)

    if proj_reset:
        LOGGER.info('Expecting GL to reset approval rules, nothing else to do.')
        return True
    LOGGER.info('Resetting approval rules via API.')
    if misc.is_production_or_staging():
        gl_mergerequest.reset_approvals()
    return False


def notify_reviewers(
    session: 'SessionRunner',
    gl_mergerequest: 'ProjectMergeRequest',
    notifications: list[tuple]
) -> None:
    """Leave a bulk 'please review this mr' comment for reviewers for affected subsystems."""
    aff_ss = ''
    body = ''
    missing_cc_emails = None
    for subsystem, num_req, reviewers in notifications:
        # If this is the goofy notification for unmapped Cc: emails then stash the email address
        # list and move on.
        if subsystem == MISSING_CC_EMAILS_NAME:
            missing_cc_emails = reviewers
            continue
        aff_ss += f'{subsystem}, '
        usernames = ' '.join([f'@{x}' for x in reviewers])
        notification = (f'Requesting review of subsystem {subsystem} with {num_req} required '
                        f'approval(s) from user(s) {usernames}\n\n')
        body += notification

    if not aff_ss and not missing_cc_emails:
        return

    notification = ''
    if aff_ss:
        notification += 'Affected subsystem(s): ' + aff_ss.strip(', ') + '\n\n' + body
    if missing_cc_emails:
        notification += ('Notice: the following email address(es) found in the MR Description Cc:'
                         f' tags could not be mapped to Gitlab usernames: {missing_cc_emails}\n')
    session.update_webhook_comment(gl_mergerequest, notification)


def purge_stale_approval_rules(
        approve_mr: 'ApproveMR',
        owners_parser: 'Parser',
        path_list: list
) -> None:
    """Purge any approval rules that are no longer valid on this MR."""
    if not path_list:
        return

    mr_subsystems = owners_parser.get_matching_subsystems(path_list)
    mr_subsystem_labels = [s.subsystem_label for s in mr_subsystems if s.subsystem_label]

    # We need all known subsystems to make sure the rule isn't a custom one
    all_subsystems = owners_parser.subsystems
    all_subsystem_labels = [s.subsystem_label for s in all_subsystems if s.subsystem_label]

    for name, rule in approve_mr.approval_rules.items():
        # The second clause should allow custom rules to not be purged
        if name not in mr_subsystem_labels and name in all_subsystem_labels:
            LOGGER.info("Deleting stale approval rule for subsystem %s", name)
            if misc.is_production_or_staging():
                approve_mr.gl_mr.approval_rules.delete(rule.id)


@dataclass(repr=False)
class ApproveMR(OwnersMixin, DependsMixin, CommitsMixin, ApprovalsMixin, DiffsMixin, BaseMR):
    """Represent the MR, its revisions and approvals."""

    @cached_property
    def project_approval_rules(self) -> typing.List['ProjectApprovalRule']:
        """Return the list of project-level approval rules."""
        return self.gl_project.approvalrules.list()

    def project_rule_required(self, rulename: str) -> typing.Union['ProjectApprovalRule', None]:
        """Evaluate rule existence and if it's required."""
        for rule in self.project_approval_rules:
            if rule.name == rulename:
                if rule.approvals_required == 0:
                    LOGGER.warning("Project %s isn't requiring any approvals for rule %s",
                                   self.gl_project.name, rulename)
                    return None
                return rule
        LOGGER.warning("Project %s has no approval rule '%s'", self.gl_project.name, rulename)
        return None

    def has_unapproved_bot_rule(self) -> bool:
        """Check for if we need to account for a missing bot approval."""
        for name, rule in self.approval_rules.items():
            if name == defs.BOT_APPROVAL_RULE:
                LOGGER.debug("%s approval rule: %s", name, rule)
                return not rule.approved

        # If the rule doesn't exist, then we're not missing an approval.
        return False

    def ensure_bot_approval_rule(self) -> None:
        """Make sure the bot approval rule exists, if it's supposed to."""
        if self.is_dependency:
            return

        pname = self.gl_project.name

        if not (proj_rule := self.project_rule_required(defs.BOT_APPROVAL_RULE)):
            return

        reset_ar = False

        for name, rule in self.approval_rules.items():
            if name == defs.BOT_APPROVAL_RULE:
                if rule.required == 0:
                    reset_ar = True
                else:
                    LOGGER.debug("%s MR %d has '%s' rule set appropriately",
                                 pname, self.iid, defs.BOT_APPROVAL_RULE)

                if reset_ar and misc.is_production_or_staging():
                    update_rule = self.gl_mr.approval_rules.get(rule.id)
                    update_rule.approvals_required = proj_rule.approvals_required
                    update_rule.save()

                return

        LOGGER.warning("%s MR %d had no bot approvals required", pname, self.iid)
        if not misc.is_production_or_staging():
            return

        mr_data = {
            'name': proj_rule.name,
            'usernames': [defs.KWF_BOT_ACCOUNT],
            'approvals_required': proj_rule.approvals_required
        }
        self.gl_mr.approval_rules.create(mr_data)

    def ensure_base_approval_rule(self) -> None:
        """Ensure base approval rule exists and has right number of reviewers set."""
        if self.is_dependency:
            return

        if not (proj_rule := self.project_rule_required(defs.ALL_MEMBERS_APPROVAL_RULE)):
            return

        reset_ar = False

        for name, rule in self.approval_rules.items():
            if name == defs.ALL_MEMBERS_APPROVAL_RULE:
                if rule.required < proj_rule.approvals_required:
                    # author access level, must be >= gitlab.MAINTAINER_ACCESS (40)
                    aal = common.get_authlevel(self.gl_project, self.gl_mr.author['id'])
                    if self.target_branch == "main":
                        LOGGER.warning("%s MR %d (branch: main) had approvals required set to %d",
                                       self.gl_project.name, self.iid, rule.required)
                        reset_ar = True
                    elif aal < MAINTAINER_ACCESS:
                        LOGGER.warning("%s MR %d (branch: %s) had approvals required set to %d by "
                                       "non-maintainer",
                                       self.gl_project.name, self.iid,
                                       self.target_branch, rule.required)
                        reset_ar = True
                    else:
                        LOGGER.debug("%s MR %d (branch: %s) has '%s' rule adjusted by "
                                     "maintainer", self.gl_project.name, self.iid,
                                     self.target_branch, defs.ALL_MEMBERS_APPROVAL_RULE)
                else:
                    LOGGER.debug("%s MR %d has '%s' rule set appropriately",
                                 self.gl_project.name, self.iid,
                                 defs.ALL_MEMBERS_APPROVAL_RULE)

                if reset_ar and misc.is_production_or_staging():
                    update_rule = self.gl_mr.approval_rules.get(rule.id)
                    update_rule.approvals_required = proj_rule.approvals_required
                    update_rule.save()

                return

        LOGGER.warning("%s MR %d had no base approvals required",
                       self.gl_project.name, self.iid)
        if not misc.is_production_or_staging():
            return

        mr_data = {
            'approval_project_rule_id': proj_rule.id,
            'name': proj_rule.name,
            'approvals_required': proj_rule.approvals_required
        }
        self.gl_mr.approval_rules.create(mr_data)

    def recheck_base_approval_rule(self) -> None:
        """Make sure any updates we made actually took (GL had issues in the past...)."""
        if not misc.is_production_or_staging() or self.gl_mr.draft:
            return

        for name, rule in self.approval_rules.items():
            if name == defs.ALL_MEMBERS_APPROVAL_RULE:
                if rule.required == 0 and self.target_branch == "main":
                    self.gl_mr.discussions.create({'body': '*ERROR*: MR has 0 required approvals'})
                return

        self.gl_mr.discussions.create({'body': '*ERROR*: MR has no '
                                       f'"{defs.ALL_MEMBERS_APPROVAL_RULE}" approval rule'})


def process_merge_request(
    session: SessionRunner,
    approve_mr: 'ApproveMR'
) -> None:
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements
    """Process a merge request."""
    labels_to_add = set()

    LOGGER.info("Got approval rule data: %s", approve_mr.approval_rules)
    LOGGER.info("Got approved by: %s", approve_mr.approved_by)
    gl_mergerequest = approve_mr.gl_mr

    # Handle adding the Config label.
    if approve_mr.config_items:
        labels_to_add.add(defs.CONFIG_LABEL)
    changed_files = approve_mr.all_files
    LOGGER.info('changed_files: %s', changed_files)

    approve_mr.ensure_bot_approval_rule()
    approve_mr.ensure_base_approval_rule()
    approve_mr.recheck_base_approval_rule()
    purge_stale_approval_rules(approve_mr, session.owners, changed_files)

    required, notifications = _get_reviewers(approve_mr, changed_files, session.owners)
    notify_reviewers(session, gl_mergerequest, notifications)

    # If CC_TAGGED_USERS_RULE_NAME is in the 'notifications' tuple then assign them as reviewers.
    if cc_usernames := next((ss[2] for ss in notifications if ss[0] == CC_TAGGED_USERS_RULE_NAME),
                            None):
        _assign_reviewers(gl_mergerequest, cc_usernames)

    ar_reviewers = get_reviewers_from_approval_rules(approve_mr)
    LOGGER.info("Approval Rules Reviewers: %s", ar_reviewers)

    new_rev_label = None
    rev_count = len(approve_mr.history)
    faked_reset = False
    if rev_count > 1:
        new_rev_label = f'{defs.CODE_CHANGED_PREFIX}v{rev_count}'
        new_rev_label = new_rev_label if new_rev_label not in gl_mergerequest.labels else None
        diff = approve_mr.code_changes()

        if new_rev_label and diff:
            interdiff = cdlib.assemble_interdiff_markdown(diff)
            faked_reset = reset_merge_request_approvals(session, approve_mr, interdiff)

    status, subsystems_acks_labels, message = _get_approval_summary(approve_mr, required,
                                                                    ar_reviewers, faked_reset)

    labels_to_add.update(subsystems_acks_labels)
    if new_rev_label:
        labels_to_add.add(new_rev_label)
    _save(session, approve_mr, status, list(labels_to_add), message)


def handle_block_action(
    session: 'SessionRunner',
    gl_mergerequest: 'ProjectMergeRequest',
    block_action: str | None,
    author: str,
    author_id: int
) -> None:
    """Create or remove blocking approval for the note author."""
    # pylint: disable=too-many-arguments
    if block_action is None:
        return

    rule_name = f"{defs.BLOCKED_BY_PREFIX} {author}"
    current_rules_iter = gl_mergerequest.approval_rules.list(iterator=True)

    if gl_mergerequest.author['username'] == author:
        block_comment = (f"WARNING: @{author}, you cannot use block actions on your own Merge "
                         "Request, because this project does not allow self-approval of MRs. You "
                         "can move your MR back to a draft, or open a blocking thread instead.")
        session.update_webhook_comment(gl_mergerequest, block_comment,
                                       bot_name=session.gl_user.username,
                                       identifier="*Block/Unblock Action:")
        LOGGER.warning("Attempt by %s to use a block action on their own MR", author)
        return

    if block_action == "block":
        block_comment = f"@{author} has blocked this Merge Request via a `/block` action."

        session.update_webhook_comment(gl_mergerequest, block_comment,
                                       bot_name=session.gl_user.username,
                                       identifier="*Block/Unblock Action:")

        if any(rule for rule in current_rules_iter if rule.name == rule_name):
            LOGGER.info("Blocking approval for user %s already exists", author)
            return
        LOGGER.info("Create blocking approval for user %s", author)

        if not misc.is_production_or_staging():
            return

        ar_data = {
            "name": rule_name,
            "approvals_required": 1,
            "user_ids": [author_id]
        }
        gl_mergerequest.approval_rules.create(data=ar_data)
        _assign_reviewers(gl_mergerequest, [f'@{author}'])

    elif block_action == "unblock":
        # Find the rule w/name matching subsystem, if any
        if not (rule := next((r for r in current_rules_iter if r.name == rule_name), None)):
            return

        LOGGER.info("Deleting blocking approval rule for user %s", author)
        if misc.is_production_or_staging():
            gl_mergerequest.approval_rules.delete(rule.id)


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    if not (gl_mr := event.gl_mr):
        return

    if event.kind is defs.GitlabObjectKind.NOTE:
        block_action = None
        for match in event.match_note_text:
            if block_action := match.groupdict().get('action'):
                handle_block_action(session, gl_mr, block_action,
                                    event.user, event.body['user']['id'])
                continue
        if not block_action and not event.has_requested_evaluation:
            LOGGER.info('Note event does not show any relevant changes, ignoring.')
            return

    approve_mr = ApproveMR.new(session, event.mr_url, source_path=session.args.rhkernel_src)
    process_merge_request(session, approve_mr)


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory where rh kernel will be checked out')
    args = parser.parse_args(args)
    session = SessionRunner.new('ack_nack', args=args, handlers=HANDLERS)
    session.run()


if __name__ == '__main__':
    main(sys.argv[1:])
