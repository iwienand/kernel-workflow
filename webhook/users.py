"""Library to represent a GL user."""
import dataclasses
import typing

from cki_lib.logger import get_logger

from . import cache

if typing.TYPE_CHECKING:
    from .session import BaseSession

LOGGER = get_logger('cki.webhook.users')


@dataclasses.dataclass(kw_only=True, unsafe_hash=True)
class User:
    """A GL user."""

    emails: list = dataclasses.field(default_factory=list, compare=False)
    gid: str = ''
    name: str = ''
    username: str = ''
    user_dict: dataclasses.InitVar[dict] = {}

    def __eq__(self, other):
        """Return True if the usernames are equal, otherwise false."""
        return self.username == other.username

    def __post_init__(self, user_dict):
        """Fix up the username as needed."""
        if user_dict:
            self.update(user_dict)
        if not self.username:
            raise ValueError('username must be set.')
        LOGGER.debug('Created %s', self)

    def __repr__(self):
        """Say who you are."""
        return f"<User '{self.username}', id: {self.id}>"

    def add_email(self, email):
        """Append an email if it doesn't already exist."""
        if email and email not in self.emails:
            self.emails.append(email)

    @property
    def id(self):
        # pylint: disable=invalid-name
        """Return the id from the gid as an int."""
        return int(self.gid.rsplit('/', 1)[-1]) if self.gid else 0

    def update(self, user_dict):
        """Update ourself from the input dict."""
        dict_copy = user_dict.copy()
        # owners.yaml Entry dicts use gluser instead of username.
        if 'username' not in dict_copy and 'gluser' in dict_copy:
            dict_copy['username'] = dict_copy.pop('gluser')
        # If the username in the dict doesn't match this object's existing username then boom.
        if self.username and self.username != dict_copy['username']:
            raise ValueError(('The dict username and object username do not match: '
                             f"{dict_copy['username']}, {self.username}'"))
        for key in {field.name for field in dataclasses.fields(self.__class__) if field.init}:
            if key in dict_copy:
                setattr(self, key, dict_copy[key])
        self.add_email(dict_copy.get('email', ''))


class UserCache(cache.BaseCache):
    """A cache for users."""

    _data_type = dict

    def __init__(
        self,
        session: 'BaseSession',
        namespace: str,
        **kwargs
    ) -> None:
        """Store the session and namespace."""
        self.session = session
        self.namespace = namespace
        super().__init__(**kwargs)

    def _find_user(self, attribute, search_key):
        """Return the found User or None."""
        if user_data := self.session.graphql.find_member(self.namespace, attribute, search_key):
            return self.get_by_dict(user_dict=user_data)
        return None

    def get(self, input_data):
        """Wrap the get_* methods."""
        if isinstance(input_data, dict):
            return self.get_by_dict(input_data)
        if isinstance(input_data, (int, str)):
            if isinstance(input_data, int) or input_data.isdigit():
                return self.get_by_id(input_data)
            if '@' in input_data:
                return self.get_by_email(input_data)
            return self.get_by_username(input_data)
        raise TypeError('input_data must be one of: dict, int, or str')

    def get_by_email(self, email):
        """Return the matching User if any, or None."""
        user = next((user for user in self.data.values() if email in user.emails), None)
        if not user:
            if user := self._find_user('email', email):
                user.add_email(email)
        return user

    def get_by_id(self, userid):
        """Return the matching User if any, or None."""
        user = next((user for user in self.data.values() if user.id == int(userid)), None)
        if not user:
            if user := self.session.graphql.get_user_by_id(int(userid)):
                return self.get_by_dict(user)
        return user

    def get_by_username(self, username):
        """Return the matching User if any, or None."""
        user = self.data.get(username)
        if not user:
            user = self._find_user('username', username)
        return user

    def get_by_dict(self, user_dict):
        """Create or update the User in the cache and then return it."""
        username = user_dict.get('username') or user_dict.get('gluser')
        if username in self.data:
            self.data[username].update(user_dict)
        else:
            self.data[username] = User(user_dict=user_dict)
        return self.data[username]

    def user_has_email(self, user, email):
        """Return True if the user is found to have the given email, otherwise False."""
        if result := \
           self.session.graphql.find_member_by_email(self.namespace, email, user.username):
            user = self.get_by_dict(user_dict=result)
            user.add_email(email)
            return True
        return False
