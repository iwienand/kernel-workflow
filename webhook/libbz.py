"""Library for interacting with bugzilla."""
from datetime import datetime
from os import environ
import typing

from bugzilla import Bugzilla
from cki_lib.logger import get_logger
from cki_lib.misc import is_production_or_staging
from cki_lib.retrying import retrying_on_exception
import prometheus_client

from webhook.defs import BZStatus
from webhook.defs import EXT_TYPE_URL

if typing.TYPE_CHECKING:
    from bugzilla.bug import Bug
    from bugzilla.oldclasses import RHBugzilla

    from webhook.rh_metadata import Branch

LOGGER = get_logger('cki.webhook.libbz')

MAX_CONNECTION_RETRIES = 2

METRIC_KWF_LIBBZ_MISSING_BUGS = prometheus_client.Counter(
    'kwf_libbz_missing_bugs', 'Number of times bugzilla api did not return any BZ data'
)

BUG_FIELDS = ['alias',
              'cf_internal_target_release',
              'cf_verified',
              'cf_zstream_target_release',
              'component',
              'depends_on',
              'external_bugs',
              'flags',
              'id',
              'priority',
              'product',
              'resolution',
              'status',
              'sub_component',
              'summary',
              'version'
              ]


def get_bzcon(api_key=environ.get('BUGZILLA_API_KEY'), host='bugzilla.redhat.com'):
    """Return a bugzilla.Bugzilla connection object."""
    return Bugzilla(host, api_key=api_key)


@retrying_on_exception(ConnectionError, retries=MAX_CONNECTION_RETRIES, initial_delay=1)
def _getbugs(bzcon, bug_list):
    """Get bug data from bugzilla and return a list of Bug objects."""
    LOGGER.info('Fetching bugzilla data for these bugs: %s', bug_list)
    return bzcon.getbugs(bug_list, include_fields=BUG_FIELDS)


def get_missing_bugs(bug_names, bug_list):
    """Return the set of IDs that are not in the list of Bug objects."""
    missing_ids = {bz_id for bz_id in bug_names if isinstance(bz_id, int) and
                   bz_id not in [bug.id for bug in bug_list]}
    missing_aliases = {alias for alias in bug_names if not isinstance(alias, int) and
                       alias not in [alias for bug in bug_list for alias in bug.alias]}
    return missing_ids | missing_aliases


def fetch_bugs(bug_list: typing.Iterable[str | int],
               bzcon: typing.Optional['RHBugzilla'] = None) -> list['Bug']:
    """Return a list of bugzilla Bug objects queried from bugzilla."""
    if not bug_list:
        LOGGER.debug('Empty input bug_list, nothing to do.')
        return []

    # Get a bugzilla connection object.
    if not bzcon:
        bzcon = get_bzcon()

    # Fetch bug data.
    bug_data = _getbugs(bzcon, bug_list)
    if missing_bzs := get_missing_bugs(bug_list, bug_data):
        LOGGER.warning('Bugzilla did not return data for these bugs: %s', missing_bzs)
        METRIC_KWF_LIBBZ_MISSING_BUGS.inc()
    return bug_data


def bugs_with_lower_status(bug_list, status, min_status=BZStatus.NEW):
    """Return the list of bugs that have a status lower than the input status."""
    return [bug for bug in bug_list if min_status <= BZStatus.from_str(bug.status) < status]


def latest_failedqa_timestamp(bug_history):
    """Return the most recent timestamp in the history when FailedQA was added to cf_verified."""
    # Surely you would only call this if the bug currently has 'FailedQA' in cf_verified.
    timestamp = None
    for update in reversed(bug_history):
        if next((change for change in update['changes'] if
                 change['field_name'] == 'cf_verified' and
                 change['added'] == 'FailedQA'), None):
            # convert the goofy xmlrpc.client.DateTime to datatime
            timestamp = datetime.strptime(update['when'].value, '%Y%m%dT%H:%M:%S')
            LOGGER.debug('FailedQA last added %s', timestamp)
            return timestamp
    LOGGER.warning('Did not find FailedQA being added to cf_verified in history.')
    return None


def bugs_to_move_to_post(bug_list, mr_pipeline_timestamp):
    """Return the bugs from the input list that need to be moved to POST."""
    # For any input bug with FailedQA we check the timestamp when FailedQA was last added to the bug
    # against the mr_pipeline_timestamp and if the former is newer we pop it from the bugs_to_update
    # list.
    # mr_pipeline_timestamp is the datetime when the MR's head pipeline finished, if any.
    bugs_to_update = bugs_with_lower_status(bug_list, BZStatus.POST)
    if not (bz_ids_to_check := [bug.id for bug in bugs_to_update if 'FailedQA' in bug.cf_verified]):
        return bugs_to_update
    if not mr_pipeline_timestamp:
        raise ValueError("The MR doesn't have a completed pipeline but a Bug has already FailedQA?")
    histories = bug_list[0].bugzilla.bugs_history_raw(bz_ids_to_check)
    bz_ids_to_remove = []
    for bz_id in bz_ids_to_check:
        if not (bug_history := next((hist for hist in histories['bugs'] if hist['id'] == bz_id),
                                    None)):
            raise ValueError(f'Failed to fetch BZ history for {bz_id}')
        if failed_qa_timestamp := latest_failedqa_timestamp(bug_history['history']):
            if failed_qa_timestamp > mr_pipeline_timestamp:
                LOGGER.debug('Bug %s got FailedQA more recently than the latest pipeline.', bz_id)
                bz_ids_to_remove.append(bz_id)
    bugs_to_update = [bug for bug in bugs_to_update if bug.id not in bz_ids_to_remove]
    return bugs_to_update


def update_bug_status(bug_list, new_status, min_status=BZStatus.NEW):
    """Change the bug status to new_status if it is currently lower, otherwise do nothing."""
    # Do nothing if the current status is lower than min_status.
    # Returns the list of bug objects which have had their status changed.
    if not bug_list:
        LOGGER.info('No bugs to update status for.')
        return []
    if not (bugs_to_update := bugs_with_lower_status(bug_list, new_status, min_status)):
        LOGGER.info('All of these bugs have status of %s or higher: %s', new_status.name,
                    [bug.id for bug in bug_list])
        return []
    bug_ids = [bug.id for bug in bugs_to_update]
    LOGGER.info('Updating status for these bugs to %s: %s', new_status.name, bug_ids)
    if not is_production_or_staging():
        for bug in bugs_to_update:
            bug.status = new_status.name
        return bugs_to_update
    bzcon = bug_list[0].bugzilla
    status_update = bzcon.build_update(status=new_status.name)
    update_result = bzcon.update_bugs(bug_ids, status_update)
    for bug in bugs_to_update:
        bz_result = next((bz for bz in update_result['bugs'] if bz['id'] == bug.id), None)
        if bz_result and bz_result['changes']['status']['added'] == new_status.name:
            bug.status = new_status.name
        else:
            raise ValueError(f'Failed to update bug status for {bug.id}: {bz_result}')
    return bugs_to_update


def _get_bz_mr_id(external_bugs, namespace):
    """Return the list of MR IDs in the BZs external trackers that match the namespace."""
    return {int(eb['ext_bz_bug_id'].split('/')[-1]) for eb in external_bugs if
            eb['type']['url'] == EXT_TYPE_URL and
            eb['ext_bz_bug_id'].startswith(f'{namespace}/-/merge_requests/')}


def get_depends_on_rt_bzs(cve_ids: typing.Iterable[str], branch: 'Branch'):
    """Return the list of depends_on bug objects from the CVEs which match the given branch."""
    # This is used to find additonal CVE clone BZs to move to POST/MODIFIED. So we're looking
    # for clones that have a component of 'kernel-rt' and otherwise match the input branch.
    depends_bz_ids = [bz_id for cve in fetch_bugs(cve_ids) for bz_id in cve.depends_on]
    attrs = {'version': branch.version}
    if branch.internal_target_release:
        attrs['cf_internal_target_release'] = branch.internal_target_release
    if branch.zstream_target_release:
        attrs['cf_zstream_target_release'] = branch.zstream_target_release
    # Get the data for the CVE tracker's depends_on BZs and then whittle the list down to just
    # the kernel-rt BZs for the same product that have an ITR, ZTR, or Version field that match
    # the given Branch.
    bug_list = fetch_bugs(depends_bz_ids)
    bug_list = [bug for bug in bug_list if
                bug.product == branch.project.product and bug.component == 'kernel-rt']
    bug_list = [bug for bug in bug_list if
                any(getattr(bug, key, None) == value for key, value in attrs.items())]
    LOGGER.info('For CVEs %s found these bugs matching branch %s: %s', cve_ids, branch.name,
                [bug.id for bug in bug_list])
    return bug_list
