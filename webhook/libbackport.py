"""Backport-specific variable definitions and functions."""
from dataclasses import dataclass
from functools import cached_property
import subprocess
import typing

from cki_lib.logger import get_logger
from git import Repo

from webhook import kgit
from webhook.rhissue import RHIssue

if typing.TYPE_CHECKING:
    from webhook.rh_metadata import Branch
    from webhook.rh_metadata import Project

LOGGER = get_logger('cki.webhook.libbackport')


@dataclass
class Backport:
    # pylint: disable=too-many-instance-attributes
    """Wrapper for a backport attempt."""

    rhissue: RHIssue
    ksrc: str = ''
    description: str = ''

    def __repr__(self) -> str:
        """Print out a user-friendly summary of the backport attempt."""
        msg = (f'Backport attempt:\n'
               f' * Issue: {self.rhissue.jira_link} (Fix Version: {self.rhissue.ji_fix_version})\n'
               f' * Target branch: {self.project.name}/{self.branch.name}\n'
               f' * Project namespace: {self.project.namespace}\n'
               f' * Backport directory: {self.repo.working_tree_dir}\n'
               f' * Source branch: {self.repo.active_branch.name}\n'
               f' * Commit(s): {self.shas}')
        return f'<{self.__class__.__name__} {self.rhissue.id}\n{msg}>'

    @property
    def shas(self) -> typing.List[str]:
        """Return the list of commit SHAs to try to backport."""
        return self.rhissue.ji_commit_hashes

    @property
    def project(self) -> typing.Union['Project', None]:
        """Return the project matched to the jira fix version."""
        return self.rhissue.ji_project

    @property
    def branch(self) -> typing.Union['Branch', None]:
        """Return the branch matched to the jira fix version."""
        return self.rhissue.ji_branch

    @cached_property
    def bp_branch(self) -> str:
        """The constructed name of the backport branch we'll operate in."""
        return f"backport-{self.rhissue.id}-{self.project.name}-{self.branch.name}"

    @cached_property
    def bp_branch_save(self) -> str:
        """The constructed name of the backport branch we'll operate in."""
        return f"{self.bp_branch}-save"

    @cached_property
    def worktree_dir(self) -> str:
        """The constructed name of the git worktree directory we'll operate in."""
        return f"/{'/'.join(self.ksrc.strip('/').split('/')[:-1])}/{self.bp_branch}/"

    @cached_property
    def repo(self) -> 'Repo':
        """Prepare branch/worktree, return git Repo object where backport operations happen."""
        # This target_branch is dependent on the repo layout in utils/rh_kernel_git_repos.yml,
        # where everything is an additional remote on top of kernel-ark,
        # w/remote name == project name
        proj_target_branch = f"{self.project.name}/{self.branch.name}"
        # This is the lookaside cache we maintain for examining diffs between revisions of a
        # merge request, which we're going to create temporary worktrees off of
        LOGGER.info("Creating git worktree at %s for backport attempt, please hold",
                    self.worktree_dir)
        try:
            kgit.worktree_add(self.ksrc, self.bp_branch, self.worktree_dir, proj_target_branch)
        except subprocess.CalledProcessError:
            LOGGER.info("Worktree for %s backport already exists, reusing it...", self.rhissue.id)
            try:
                kgit.branch_delete(self.ksrc, self.bp_branch_save)
            except subprocess.CalledProcessError:
                pass
            kgit.branch_copy(self.worktree_dir, self.bp_branch_save)
            kgit.hard_reset(self.worktree_dir, proj_target_branch)

        return Repo(self.worktree_dir)
